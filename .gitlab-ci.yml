---
stages:
  - lint
  - clean
  - network
  - servers
  - servers_config

##
# Variables
##

variables:
  admin_openrc_path: vars/openstack_openrc
  GIT_SUBMODULE_STRATEGY: recursive
  ANSIBLE_DOCKER_IMAGE:
    registry.gitlab.com/orange-opensource/lfn/ci_cd/docker_azure_ansible
  ANSIBLE_DOCKER_TAG: "2.9.6-alpine"
  CHAINED_CI_INIT: scripts/chained-ci-init.sh
  INVENTORY_FILE: localhost

.syntax_checking: &syntax_checking
  stage: lint
  except:
    - schedules
    - triggers
    - web
  tags:
    - docker

yaml_checking:
  image: sdesbure/yamllint:latest
  script:
    - yamllint *.yml
    - yamllint inventory/group_vars/*.yml
  <<: *syntax_checking

ansible_linting:
  image: sdesbure/ansible-lint:latest
  script:
    - ansible-lint -x ANSIBLE0010,ANSIBLE0013 os_*.yml
  <<: *syntax_checking

##
# Generic
##

.ansible_run: &ansible_run
  tags:
    - docker
  image: ${ANSIBLE_DOCKER_IMAGE}:${ANSIBLE_DOCKER_TAG}
  only:
    - schedules
    - triggers
    - web

.chained_ci_tools_after: &chained_ci_tools_after
  after_script:
    - ./scripts/clean.sh
    - rm $HOME/.azure/credentials || true

.chained_ci_tools_before: &chained_ci_tools_before
  <<: *chained_ci_tools_after
  before_script:
    - chmod 600 .
    - . ./${CHAINED_CI_INIT} -a -i inventory/$INVENTORY_FILE
    - >
      ansible-playbook ${ansible_verbose} ${VAULT_OPT} -i inventory/$INVENTORY_FILE \
        az_credentials.yml
.artifacts: &artifacts
  artifacts:
    paths:
      - inventory/localhost
      - vars/pdf.yml
      - vars/idf.yml
      - vars/user_cloud.yml
      - vars/vaulted_ssh_credentials.yml
      - vars/ssh_gateways.yml
      - inventory/infra

# clean when user (only known networks, servers and volumes)
clean:
  stage: clean
  <<: *ansible_run
  <<: *chained_ci_tools_before
  only:
    variables:
      - $CLEAN
      - $ONLY_CLEAN
    refs:
      - triggers
  script:
    - >
      ansible-playbook ${ansible_verbose} ${VAULT_OPT} -i inventory/$INVENTORY_FILE \
        az_clean.yml

create_network:
  stage: network
  <<: *ansible_run
  <<: *chained_ci_tools_before
  <<: *artifacts
  except:
    variables:
      - $ONLY_CLEAN
  script:
    - >
      ansible-playbook ${ansible_verbose} ${VAULT_OPT} -i inventory/$INVENTORY_FILE \
        az_network.yml

create_servers:
  stage: servers
  <<: *ansible_run
  <<: *chained_ci_tools_before
  <<: *artifacts
  except:
    variables:
      - $ONLY_CLEAN
  script:
    - >
      ansible-playbook ${ansible_verbose} ${VAULT_OPT} -i inventory/$INVENTORY_FILE \
        az_servers.yml
  dependencies:
    - create_network

set_keys:
  stage: servers_config
  <<: *ansible_run
  <<: *chained_ci_tools_before
  <<: *artifacts
  variables:
    INVENTORY_FILE: infra
  except:
    variables:
      - $ONLY_CLEAN
  script:
    - >
      ansible-playbook ${ansible_verbose} ${VAULT_OPT} -i inventory/$INVENTORY_FILE \
        az_set_keys.yml
  dependencies:
    - create_servers

add_trim:
  stage: servers_config
  <<: *ansible_run
  <<: *chained_ci_tools_before
  <<: *artifacts
  variables:
    INVENTORY_FILE: infra
  except:
    variables:
      - $ONLY_CLEAN
  script:
    - >
      ansible-playbook ${ansible_verbose} ${VAULT_OPT} -i inventory/$INVENTORY_FILE \
        az_set_trim.yml
  dependencies:
    - create_servers
